
import './App.css';
import Nav from './Nav';
import { Fragment } from 'react';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props)  {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <Fragment>
      <BrowserRouter>




      <div className="container">
       {/* <ConferenceForm /> */}
      {/* <LocationForm /> */}
      {/* <AttendeesList attendees={props.attendees} /> */}
        <Nav />
        <Routes>
          <Route index element={<MainPage />}></Route>
          <Route path='/conferences/new' element={<ConferenceForm/>}></Route>
          <Route path='/attendees/new' element={<AttendConferenceForm/>}></Route>
          <Route path='/locations/new' element={<LocationForm/>}></Route>
          <Route path='/attendees' element={<AttendeesList attendees={props.attendees}/>}></Route>
          <Route path='/presentations' element={<PresentationForm/>}></Route>
        </Routes>
      </div>
    </BrowserRouter>
    </Fragment>
  );
}

export default App;
