import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {

    const [locations, setLocations] = useState([])
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentation, setMaxPresentation] = useState('');
    const [max_attendee, setMaxAttendee] = useState('');
    const [location, setLocation] = useState('');

    const handleLocation = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleMaxAttendee = (event) => {
        const value = event.target.value;
        setMaxAttendee(value);
    }

    const handleMaxPresentation = (event) => {
        const value = event.target.value;
        setMaxPresentation(value);
    }

    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleEnds = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleStarts = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async (event) => {
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentation = max_presentation;
        data.max_attendee = max_attendee;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
              const newConference = await response.json();
              console.log(newConference);

              setName('');
              setStarts('');
              setEnds('');
              setDescription('');
              setMaxPresentation('');
              setMaxAttendee('');
              setLocation('');

            }
          }
          const fetchData = async () => {
            const url = 'http://localhost:8000/api/locations/';


            const response = await fetch(url);

            if (response.ok) {
              const data = await response.json();
              setLocations(data.locations)
              }
            }
                useEffect(() => {
                  fetchData();
                }, []);



return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStarts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="date">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEnds} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="date">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescription} className="form-control" name="description" id="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentation} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendee} placeholder="Maximum Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocation} required type="number" name="location" id="location" className="form-select">
                  <option value="">Choose a Location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.name}>
                                {location.name}
                            </option>
                        )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )

}
export default ConferenceForm;
