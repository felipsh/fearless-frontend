import React, { useEffect, useState } from 'react';

function PresentationForm(){

    const [conferences, setConferences] = useState('');
    const [presenter_name, setName] = useState("");
    const [email, setEmail] = useState('');
    const [company_name, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const handleConference = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleCompanyName = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const handleEmail = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async (event) => {
        const data = {};

        data.presenter_name = presenter_name;
        data.email = email;
        data.company_name = company_name;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        const presentationUrl = 'http://localhost:8000/api/presentations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
              const newPresentation = await response.json();
              console.log(newPresentation);

              setName('');
              setEmail('');
              setCompanyName('');
              setTitle('');
              setSynopsis('');
              setConference('');

            }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';


        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences)
          }
        }
            useEffect(() => {
              fetchData();
            }, []);



    return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmail} placeholder="Email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyName} placeholder="Company Name" required type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="Company Name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="Synopsis" className="form-label">Synopsis</label>
                <textarea onChange={handleSynopsis} className="form-control" name="synopsis" id="synopsis" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConference} required name="conference" id="conference" className="form-select">
                  <option value="id">Choose a Conference</option>
                  {Object.values(conferences).map(conference => {
                        return (
                            <option key={conference.id} value={conference.id}>
                                {conference.name}

                            </option>
                        )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    );
}
export default PresentationForm;
